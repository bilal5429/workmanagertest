package com.example.foregroundserviceexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import android.os.Bundle;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button startButton = (Button) findViewById(R.id.button1);
        Button stopButton = (Button) findViewById(R.id.button2);
        startButton.setOnClickListener(this);
        stopButton.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
//                Intent startIntent = new Intent(MainActivity.this, ForegroundService.class);
//                startIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
//                startService(startIntent);

//                WorkRequest uploadWorkRequest =
//                        new OneTimeWorkRequest.Builder(UploadWorker.class)
//                                .build();

                final PeriodicWorkRequest periodicWorkRequest
                        = new PeriodicWorkRequest.Builder(UploadWorker.class, 2, TimeUnit.SECONDS)
                        .build();

                WorkManager
                        .getInstance(this)
                        .enqueue(periodicWorkRequest);

                break;
            case R.id.button2:
                Intent stopIntent = new Intent(MainActivity.this, ForegroundService.class);
                stopIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
                startService(stopIntent);
                break;
            default:
                break;
        }
    }

}