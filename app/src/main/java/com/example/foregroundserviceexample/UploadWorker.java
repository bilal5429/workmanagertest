package com.example.foregroundserviceexample;

import android.content.Context;


import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class UploadWorker extends Worker {
    public UploadWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
    }

    @Override
    public Result doWork() {

        // Do the work here--in this case, upload the images.
        System.out.println("Hello from worker class");
//        Toast.makeText(getApplicationContext(),"Hello cat",Toast.LENGTH_LONG).show();
        // Indicate whether the work finished successfully with the Result
        return Result.retry();
    }
}